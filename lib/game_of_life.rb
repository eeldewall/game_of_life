class GameOfLife
  def self.decide_fate(cell)
    if cell.dead? 
      cell.revive if cell.neighbours == 3
    else        
      cell.kill if cell.neighbours > 3 or cell.neighbours < 2
    end
  end
end
