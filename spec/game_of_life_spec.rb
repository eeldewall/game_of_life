require_relative "../lib/game_of_life.rb"

describe GameOfLife do

  describe "decides fate of a cell" do
    let(:neighbours) { double("Neighbours") }

    describe "for a living cell" do
      let(:cell) { double("Cell", :dead? => false, :neighbours => neighbours) }

      it "kills living cells with fewer then 2 neighbours" do
        neighbours.stub(:<).with(2).and_return(true)
        neighbours.stub(:>).with(3).and_return(false)
        cell.should_receive(:kill)
        GameOfLife.decide_fate(cell)
      end

      it "lets living cells live if two or three neighbours" do
        neighbours.stub(:<).with(2).and_return(false)
        neighbours.stub(:>).with(3).and_return(false)
        cell.should_not_receive(:kill)
        GameOfLife.decide_fate(cell)
      end

      it "kills living cells with more then three neighbours" do
        neighbours.stub(:>).with(3).and_return(true)
        cell.stub(:neighbours).and_return(neighbours)
        cell.should_receive(:kill)
        GameOfLife.decide_fate(cell)
      end
    end

    describe "for a dead cell" do

      let(:cell) { double("Cell", :dead? => true, :neighbours => neighbours) }

      it "revives a dead cell if it has three neighbours" do
        neighbours.stub(:==).with(3).and_return(true)
        cell.should_receive(:revive)
        GameOfLife.decide_fate(cell)
      end

      it "does not revive a cell with two neighbours" do
        neighbours.stub(:==).with(3).and_return(false)
        cell.should_not_receive(:revive)
        GameOfLife.decide_fate(cell)
      end
    end
  end
end

