require_relative "../lib/game_of_life.rb"

class Game
  
  def initialize universe

  end

  def evolve_universe
    GameOfLife.decide_fate Object.new
  end
end

describe Game do

  it "creates new generations of a universe of cells based on the GameOfLife rules" do
    cells = [Object.new]
    universe = double("Universe", :cells => cells)

    game = Game.new(universe)
    next_gen = game.evolve_universe

    GameOfLife.should_receive(:decide_fate).exactly(cells.count).times
  end
end
